
Pod::Spec.new do |s|

  s.name         = "SKButtonNode"
  s.version      = "1.0.0"
  s.summary      = "A simple button node to SpriteKit"
  s.description  = "The SKButtonNode, it is a custom SKNode to easy add Buttons on SpriteKit Scenes."
  s.homepage     = "https://gitlab.com/vfvasconcelos3010/skbuttonnode"

  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.license      = "MIT"

  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.author             = { "Victor Vasconcelos" => "vfvasconcelos3010@gmail.com" }

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.platform = :ios
  s.platform = :tvos

  s.ios.deployment_target = '12.2'
  s.tvos.deployment_target = '9.0'

  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.source       = { :git => "https://gitlab.com/vfvasconcelos3010/skbuttonnode.git", :tag => "#{s.version}" }
  # s.source       = { :git => "https://gitlab.com/vfvasconcelos3010/skbuttonnode.git", :branch => "master" }

  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.source_files = "SKButtonNode/*"
  # s.source_files = "*.podspec"
  # s.source_files = "SKButtonNode/*.podspec"
  # s.source_files = "SKButtonNode"

  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.swift_version = "5.0" 
end
