//
//  SKButtonNode.swift
//  SKButtonNode
//
//  Created by Victor Vasconcelos on 10/04/19.
//  Copyright © 2019 FF Studios. All rights reserved.
//

import Foundation
import SpriteKit

/// A custom SKNode to act like a button in SpriteKit
public class SKButtonNode: SKNode {
    
    /// Node to represent background image for the button
    public var backgroundNode: SKSpriteNode
    

    /// Label Node
    /**
     * This is a label node to goes inside the Button.
     *
     * You can pass a empety string, but in this case the button won't show any label.
    */
    public var labelNode: SKLabelNode
    
    /// Clousre to do something when button is clicked
    /**
     * For default this is nil, because of this click on button won't do nothing.
     */
    public var action: (() -> Void)?
    
    
    /// Empety initializer
    /**
     
     * This will create a default button blue with 100x100 of Size, and a label on center of Node with no action
     */
    public override init() {
        self.backgroundNode = SKSpriteNode(color: .blue, size: CGSize(
            width: 100, height: 100))
        self.labelNode = SKLabelNode(text: "I'm a Button")
        super.init()
        setupButton()
    }
    
    public init(imageNamed: String, label: String = "", action: @escaping () -> Void = {}) {
        self.backgroundNode = SKSpriteNode(imageNamed: imageNamed)
        self.labelNode = SKLabelNode(text: label)
        self.action = action
        super.init()
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if action != nil {
            action!()
        }
    }
    
    private func setupButton() {
        
        // Multiple lines
        self.labelNode.numberOfLines = 0
        // To fix label inside the Button with a offset = 30
        self.labelNode.preferredMaxLayoutWidth = self.backgroundNode.size.width - 30
        
        // Ajust zPosition
        self.backgroundNode.zPosition = -5
        self.labelNode.zPosition = self.backgroundNode.zPosition + 5
        
        self.isUserInteractionEnabled = true
        self.addChild(self.backgroundNode)
        self.addChild(self.labelNode)
    }
}
